"use strict"

const events = require('events')
const ws = require('ws')

module.exports.Socket = function Socket(uri) {
  if (!(this instanceof Socket))
    return new Socket(uri)

  this.__proto__ = Object.assign({}, events.EventEmitter.prototype)

  Object.assign(this, {
    connect: function() {
      let io = new ws(uri)

      this.out = buffer => new Promise((res, rej) => {
        io.send(buffer, err => {
          err ? rej(err) : res()
        })
      })

      io.on('close', () => this.emit('close'))

      io.on('open', () => this.emit('open'))

      io.on('message', buffer => this.emit('data', buffer))
    }
  })
}